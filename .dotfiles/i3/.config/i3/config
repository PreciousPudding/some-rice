# Please see https://i3wm.org/docs/userguide.html for a complete reference!

set $mod Mod4

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
font pango: Hack Nerd Font 8
title_align center

# This font is widely installed, provides lots of unicode glyphs, right-to-left
# text rendering and scalability on retina/hidpi displays (thanks to pango).
#font pango:DejaVu Sans Mono 8

# The combination of xss-lock, nm-applet and pactl is a popular choice, so
# they are included here as an example. Modify as you see fit.

# xss-lock grabs a logind suspend inhibit lock and will use i3lock to lock the
# screen before suspend. Use loginctl lock-session to lock your screen.
#exec_always      --no-startup-id xss-lock --transfer-sleep-lock -- 'betterlockscreen --lock dimblur -t "Please type your password"' #-- i3lock --nofork

# NetworkManager is the most popular way to manage wireless networks on Linux,
# and nm-applet is a desktop environment-independent system tray GUI for it.
exec --no-startup-id nm-applet

# Use pactl to adjust volume in PulseAudio.
exec_always                        --no-startup-id     "~/.local/bin/other/pulse-xob  | xob      -c ~/.config/xob/styles.cfg         -s i3 &"       # Volume meter via xob
bindsym XF86AudioRaiseVolume        exec --no-startup-id pactl set-sink-volume    @DEFAULT_SINK@     +2%
bindsym XF86AudioLowerVolume        exec --no-startup-id pactl set-sink-volume    @DEFAULT_SINK@     -2%
bindsym XF86AudioMute               exec --no-startup-id pactl set-sink-mute      @DEFAULT_SINK@      toggle
bindsym XF86AudioMicMute            exec --no-startup-id pactl set-source-mute    @DEFAULT_SOURCE@    toggle

# Sreen brightness controls
#exec                 brightnessctl set 33
bindsym XF86MonBrightnessUp exec brightnessctl s 3+ # increase screen brightness
bindsym XF86MonBrightnessDown exec brightnessctl s 3- # decrease screen brightness

# Media player controls
bindsym XF86AudioPlay exec playerctl play-pause
#bindsym XF86AudioPause exec playerctl pause # For some reason this doesn't work on the G14. I have to use XF86AudioPlay as a toggle (play-pause) instead of just a play
bindsym XF86AudioNext exec playerctl next
bindsym XF86AudioPrev exec playerctl previous

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# start a terminal
#bindsym $mod+Return exec i3-sensible-terminal
bindsym $mod+Return		exec kitty

# kill focused window
bindsym $mod+q kill

# Toggle between floating and tiling windows
bindsym $mod+space		focus mode_toggle

# start dmenu (a program launcher)
bindsym $mod+Shift+Return    exec --no-startup-id         rofi -show drun -show-icons -drun-icon-theme Paper
#bindsym $mod+d exec --no-startup-id i3-dmenu-desktop   # Only shows .desktop files

# change focus
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# alternatively, you can use the cursor keys:
#bindsym $mod+Left focus left
#bindsym $mod+Down focus down
#bindsym $mod+Up focus up
#bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# Toggle which direction windows are tiled
bindsym $mod+t			split toggle

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
#bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "I"
set $ws2 "II"
set $ws3 "III"
set $ws4 "IV"
set $ws5 "V"
set $ws6 "VI"
set $ws7 "VII"
set $ws8 "VIII"
set $ws9 "IX"
set $ws10 "X"

# switch to workspace
bindsym     $mod+Tab    workspace back_and_forth
bindsym     $mod+1      workspace $ws1
bindsym     $mod+2      workspace $ws2
bindsym     $mod+3      workspace $ws3
bindsym     $mod+4      workspace $ws4
bindsym     $mod+5      workspace $ws5
bindsym     $mod+6      workspace $ws6
bindsym     $mod+7      workspace $ws7
bindsym     $mod+8      workspace $ws8
bindsym     $mod+9      workspace $ws9
bindsym     $mod+0      workspace $ws10

bindsym    $mod+Left    workspace prev
bindsym    $mod+Right   workspace next

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8
bindsym $mod+Shift+9 move container to workspace $ws9
bindsym $mod+Shift+0 move container to workspace $ws10

# Refresh i3
#bindsym $mod+Shift+c reload
bindsym $mod+Shift+r restart

# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e    exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -B 'Yes, exit i3' 'i3-msg exit'"

# Disable window titlebars entirely
for_window [class="^.*"] border pixel 1

# Default inner/outer gaps
set $inner_gaps 9
set $outer_gaps 3

gaps inner $inner_gaps
gaps outer $outer_gaps

# Reset innter and outer gaps
bindsym $mod+Shift+t		gaps inner current set $inner_gaps; gaps outer current set $outer_gaps

# Remove gaps
bindsym $mod+Shift+d		gaps inner current set 0; gaps outer current set 0

# Change innoer and outer gaps
bindsym $mod+z			gaps outer current plus 5
bindsym $mod+Shift+z		gaps outer current minus 5

bindsym $mod+s			gaps inner current plus 5
bindsym $mod+Shift+s		gaps inner current minus 5


# Additionally, you can issue commands with the following syntax. This is useful to bind keys to changing the gap size.
# gaps inner|outer current|all set|plus|minus <px>
# gaps inner all set 10
# gaps outer all plus 5

# Smart gaps (gaps used if only more than one container on the workspace)
# smart_gaps on

# Smart borders (draw borders around container only if it is not the only container on this workspace)
# on|no_gaps (on=always activate and no_gaps=only activate if the gap size to the edge of the screen is 0)
smart_borders on

# Resizing Windows
bindsym $mod+Shift+y		exec --no-startup-id $HOME/.local/bin/i3cmds/i3resize left
bindsym $mod+Shift+u		exec --no-startup-id $HOME/.local/bin/i3cmds/i3resize down
bindsym $mod+Shift+i		exec --no-startup-id $HOME/.local/bin/i3cmds/i3resize up
bindsym $mod+Shift+o		exec --no-startup-id $HOME/.local/bin/i3cmds/i3resize right

# Window Colors
set $bgcolor    #7accd7CC
set $ibgcolor		#2d2a2e
set $fws		#00897b80
set $barcolor		#00897b4D
set $textcolor		#ffffff
set $ubgcolor		#f85e84
#                            border       background      text            indicator       child_border
client.focused               $bgcolor     $bgcolor        $textcolor      $bgcolor
client.unfocused             $ibgcolor    $ibgcolor       $textcolor      $ibgcolor
client.focused_inactive      $ibgcolor    $ibgcolor       $textcolor      $ibgcolor
client.urgent                $ubgcolor    $ubgcolor       $textcolor      $ubgcolor

# Display Wallpaper
#exec_always      feh --bg-fill ~/Pictures/cherry_blossoms.png # Simply using feh
exec_always       --no-startup-id betterlockscreen -w

# Keybindings for i3lock
bindsym $mod+x      exec betterlockscreen --lock dimblur -t "Please type your password" --show-layout

# Screensave (xidlehook)
#exec_always       --no-startup-id xidlehook --not-when-audio --timer 420 "betterlockscreen -l dimblur -t 'Please type your password'" ""
#exec_always       --no-startup-id xidlehook --not-when-audio --timer 780 "systemctl suspend" ""

# Assign Programs to Workspaces
#for_window [class="Brave-browser"]     move to workspace $ws1
#for_window      [class="discord"]            move to workspace $ws2
#for_window      [class="Skype"]                move to workspace $ws2
#for_window      [class='qBittorrent']         move to workspace $ws9
#for_window      [class='Franz']               move to workspace $ws2

# Set up programs at log in/startup
# Nothing here yet!

# Window Compositor
exec_always               picom -bc --backend xrender

# Polybar Launch Script
exec_always --no-startup-id $HOME/.config/polybar/launch.sh

# Launch Flameshot
bindsym Ctrl+Shift+s      exec --no-startup-id    flameshot gui

# Deadd notification center
#exec                      --no-startup-id           "pkill -9 -f deadd-notification-center ; deadd-notification-center"
#exec_always                      --no-startup-id           deadd-notification-center
bindsym     $mod+d        exec --no-startup-id      kill -s USR1 $(pidof deadd-notification-center)

# Send notification on fanmode change
#exec_always               --no-startup-id      pkill -9 -f watch_fanmode.s
#exec_always               --no-startup-id      $HOME/.local/bin/other/watch_fanmode.sh

# Notify on low battery
#exec_always               --no-startup-id      pkill -9 -f batt_noti.sh
#exec_always               --no-startup-id      $HOME/.local/bin/other/batt_noti.sh

# Switch between QWERTY and Colemak-DH
exec                               --no-startup-id        "setxkbmap -model p105 -layout us -option 'misc:extend,lv5:caps_switch_lock,compose:menu"
bindsym     $mod+Ctrl+Left         exec --no-startup-id   "setxkbmap -model p105 -layout us -option 'misc:extend,lv5:caps_switch_lock,compose:menu' ; notify-send 'Keyboard' 'Keyboard layout changed to <b>standard QWERTY~!</b>'"
bindsym     $mod+Ctrl+Right        exec --no-startup-id   "setxkbmap -model pc105 -layout us -variant cmk_ed_us -option 'misc:extend,lv5:caps_switch_lock,compose:menu' ; notify-send 'Keyboard' 'Keyboard layout changed to <b>Colemak-DH wide~!</b>'"

# Start fcitx for Korean
#exec_always           --no-startup-id fcitx

# Start rog-watcher
#exec_always           --no-startup-id     rog-watcher

# Bluetooth device selection
bindsym     $mod+b                exec --no-startup-id    rofi-bluetooth


# Fix keyboard backlight issue with asus-nb-ctrl
bindsym     XF86KbdBrightnessUp        exec light -s sysfs/leds/asus::kbd_backlight -A 50
bindsym     XF86KbdBrightnessDown      exec light -s sysfs/leds/asus::kbd_backlight -U 50

# On startup
exec         --no-startup-id           $HOME/.local/bin/other/startup
#exec        --no-startup-id           i3-msg 'workspace I; exec brave &'
#exec        --no-startup-id           i3-msg 'workspace II; exec obsidian &'
#exec        --no-startup-id           i3-msg 'workspace II; exec Everdo &'
#exec        --no-startup-id           i3-msg 'workspace IX; exec pavucontrol &'
#exec        --no-startup-id           i3-msg 'workspace X; exec discord &'
#exec        --no-startup-id           i3-msg 'workspace X; exec Skype &'

# For the Church of Emacs
bindsym     $mod+o               exec     "emacs --with-profile debug --with-debug-init"
bindsym     $mod+i               exec     "emacs --with-debug-init"
bindsym     $mod+v               exec     "emacsclient -c"
