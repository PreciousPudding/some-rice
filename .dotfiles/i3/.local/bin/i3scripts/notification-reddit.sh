#!/bin/sh

URL="https://www.reddit.com/message/unread/.json?feed=8f951397bfe5d6cb8f5279098451f2b048943039&user=FluentFelicity"
USERAGENT="polybar-scripts/notification-reddit:v1.0 r/FluentFelicity"

notifications=$(curl -sf --user-agent "$USERAGENT" "$URL" | jq '.["data"]["children"] | length')


cls='' 
BROWSER='/usr/bin/brave'
if [[ $BROWSER =~ '/usr/bin/brave' ]]; then
	cls='Brave-browser'
#elif [[ $BROWSER =~ 'google-chrome-stable$' ]]; then
	#cls='Google-chrome'
fi

if [ -n "$notifications" ] && [ "$notifications" -gt 0 ]; then
	# reddit-alien in reddit's colour
	# open chrome on unread messages and switch to ws1
	echo "%{A1:$BROWSER https\://www.reddit.com/message/unread/ ; i3-msg '[class=\"$cls\"]' focus:}%{F#F48FB1} $notifications%{F-}%{A}"
else
	# open reddit and switch to ws1
  echo "%{A1:$BROWSER https\://www.reddit.com/ ; i3-msg '[class=\"$cls\"]' focus:}%{F#D9DBDF}%{A}"
fi
