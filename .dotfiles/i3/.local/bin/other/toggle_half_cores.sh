#!/bin/bash
# This is a script for turning on or off threads/cores 9-16 on the Zephyrus G14

state=$(sudo cat /sys/devices/system/cpu/cpu8/online)
if [ $state = '0' ]; then
  num=1
  echo "Cores enabled!"
else 
  num=0
  echo "Cores disabled!"
fi


for thread in $(seq 8 15);
do
  echo "$num" > /sys/devices/system/cpu/cpu$thread/online
done

