" auto-install vim-plug
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  "autocmd VimEnter * PlugInstall
  "autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin('~/.config/nvim/autoload/plugged')

    " Better Syntax Support
    "Plug 'sheerun/vim-polyglot'
    " Auto pairs for '(' '[' '{'
    Plug 'jiangmiao/auto-pairs'
    " Displays keymappings by pressing space 
    Plug 'liuchengxu/vim-which-key'
    " Shows recently opened files at nvim start   
    Plug 'mhinz/vim-startify'
    " Sonokai Color Theme 
    Plug 'sainnhe/sonokai' 
    " FZF; has many plugins, they word search documents, buffers, words in
    " documents, etc; FZF's github has a list of commands
    Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
    Plug 'junegunn/fzf.vim'
    " Makes sure that FZF has 'project directory scope' - wahtever that means
    Plug 'airblade/vim-rooter' 
    " Gives :SudoWrite command
    Plug 'tpope/vim-eunuch'
    " Shows buffers in tab form and more!
    "Plug 'pacha/vem-tabline'    
    " Has a bunch of plugins (I thnk)
    Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'
    " Fully fuzzy path finder (:CtrlP)
    "Plug 'ctrlpvim/ctrlp.vim'
    " Syntax checker
    "Plug 'vim-syntastic/syntastic'
    " Make nvim have more IDE features
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
    " Adds a colorizer (highlights hex as the color it is)
    Plug 'norcalli/nvim-colorizer.lua'
    " Jump to words easier
    Plug 'justinmk/vim-sneak'
    " Ranger integration in nvim and a dependency
    Plug 'kevinhwang91/rnvimr', {'do': 'make sync'}
    " For navigating directories
    Plug 'preservim/nerdtree'
    " Adds icons for various plugins
    Plug 'ryanoasis/vim-devicons'
    " Allows commenting shortcut like in VSCode
    Plug 'preservim/nerdcommenter'    
    " Live markdown preview 
    Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}
call plug#end()


" Sonokai Theme 
if has('termguicolors')
  set termguicolors
endif

" For nvim-colorizer
lua require'colorizer'.setup()
filetype plugin on
autocmd BufReadPost *.conf setl ft=conf

" Automatically install missing plugins on startup
autocmd VimEnter *
  \  if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \|   PlugInstall --sync | q
  \| endif
