
" The configuration options should be placed before `colorscheme sonokai`.
let g:sonokai_style = 'andromeda'
let g:sonokai_enable_italic = 1
"let g:sonokai_disable_italic_comment = 1
let g:sonokai_cursor = 'red'
let g:sonokai_transparent_background = 1
let g:sonokai_menu_selection_background = 'blue'
let g:sonokai_current_word = 'bold'

colorscheme sonokai
