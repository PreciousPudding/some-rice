" Tabline plugin
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'unique_tail' 

" Airline
let g:airline_theme = 'sonokai'

